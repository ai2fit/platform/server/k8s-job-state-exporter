/*
Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package jobs

import (
	"testing"

	batchv1 "k8s.io/api/batch/v1"
)

type fakeJobHandler struct {
	onAddFunc    func(*batchv1.Job)
	onUpdateFunc func(*batchv1.Job, *batchv1.Job)
	onDeleteFunc func(*batchv1.Job)
}

func (c *fakeJobHandler) OnAdd(job *batchv1.Job) {
	if c.onAddFunc != nil {
		c.onAddFunc(job)
	}
}

func (c *fakeJobHandler) OnUpdate(oldEvent, newEvent *batchv1.Job) {
	if c.onUpdateFunc != nil {
		c.onUpdateFunc(oldEvent, newEvent)
	}
}

func (c *fakeJobHandler) OnDelete(job *batchv1.Job) {
	if c.onDeleteFunc != nil {
		c.onDeleteFunc(job)
	}
}

func TestEventWatchHandlerAdd(t *testing.T) {
	testCases := []struct {
		desc     string
		obj      interface{}
		expected bool
	}{
		{
			"obj=nil",
			nil,
			false,
		},
		{
			"obj=non-event",
			42,
			false,
		},
		{
			"obj=event",
			&batchv1.Job{},
			true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			isTriggered := false
			fakeHandler := &fakeJobHandler{
				onAddFunc: func(*batchv1.Job) { isTriggered = true },
			}

			c := newJobHandlerWrapper(fakeHandler)
			c.OnAdd(tc.obj)

			if isTriggered != tc.expected {
				t.Fatalf("Add is triggered = %v, expected %v", isTriggered, tc.expected)
			}
		})
	}
}

func TestEventWatchHandlerUpdate(t *testing.T) {
	testCases := []struct {
		desc     string
		oldObj   interface{}
		newObj   interface{}
		expected bool
	}{
		{
			"oldObj=nil,newObj=event",
			nil,
			&batchv1.Job{},
			true,
		},
		{
			"oldObj=non-event,newObj=event",
			42,
			&batchv1.Job{},
			false,
		},
		{
			"oldObj=event,newObj=nil",
			&batchv1.Job{},
			nil,
			false,
		},
		{
			"oldObj=event,newObj=non-event",
			&batchv1.Job{},
			42,
			false,
		},
		{
			"oldObj=event,newObj=event",
			&batchv1.Job{},
			&batchv1.Job{},
			true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			isTriggered := false
			fakeHandler := &fakeJobHandler{
				onUpdateFunc: func(*batchv1.Job, *batchv1.Job) { isTriggered = true },
			}

			c := newJobHandlerWrapper(fakeHandler)
			c.OnUpdate(tc.oldObj, tc.newObj)

			if isTriggered != tc.expected {
				t.Fatalf("Update is triggered = %v, expected %v", isTriggered, tc.expected)
			}
		})
	}
}

func TestEventWatchHandlerDelete(t *testing.T) {
	testCases := []struct {
		desc     string
		obj      interface{}
		expected bool
	}{
		{
			"obj=nil",
			nil,
			false,
		},
		{
			"obj=non-event",
			42,
			false,
		},
		{
			"obj=event",
			&batchv1.Job{},
			true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			isTriggered := false
			fakeHandler := &fakeJobHandler{
				onDeleteFunc: func(*batchv1.Job) { isTriggered = true },
			}

			c := newJobHandlerWrapper(fakeHandler)
			c.OnDelete(tc.obj)

			if isTriggered != tc.expected {
				t.Fatalf("Delete is triggered = %v, expected %v", isTriggered, tc.expected)
			}
		})
	}
}
