/*
Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package jobs

import (
	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/klog/v2"
)

// JobHandler interface provides a way to act upon signals
// from watcher that only watches the events resource.
type JobHandler interface {
	OnAdd(*batchv1.Job)
	OnUpdate(*batchv1.Job, *batchv1.Job)
	OnDelete(*batchv1.Job)
}

// JobHandlerWrapper is a wrapper of JobHandler for testing proposes.
type JobHandlerWrapper struct {
	handler JobHandler
}

func newJobHandlerWrapper(handler JobHandler) *JobHandlerWrapper {
	return &JobHandlerWrapper{
		handler: handler,
	}
}

func (c *JobHandlerWrapper) OnAdd(obj interface{}) {
	if event, ok := c.convert(obj); ok {
		c.handler.OnAdd(event)
	}
}

func (c *JobHandlerWrapper) OnUpdate(oldObj interface{}, newObj interface{}) {
	oldJob, oldOk := c.convert(oldObj)
	newJob, newOk := c.convert(newObj)
	if newOk && (oldObj == nil || oldOk) {
		c.handler.OnUpdate(oldJob, newJob)
	}
}

func (c *JobHandlerWrapper) OnDelete(obj interface{}) {

	event, ok := obj.(*batchv1.Job)

	// When a delete is dropped, the relist will notice a pod in the store not
	// in the list, leading to the insertion of a tombstone object which contains
	// the deleted key/value. Note that this value might be stale.
	if !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			klog.V(2).Infof("Object is neither event nor tombstone: %+v", obj)
			return
		}
		event, ok = tombstone.Obj.(*batchv1.Job)
		if !ok {
			klog.V(2).Infof("Tombstone contains object that is not a pod: %+v", obj)
			return
		}
	}

	c.handler.OnDelete(event)
}

// Coverts an object of any type to a batchv1.Job if applicable.
func (c *JobHandlerWrapper) convert(obj interface{}) (*batchv1.Job, bool) {
	if job, ok := obj.(*batchv1.Job); ok {
		return job, true
	}
	klog.V(2).Infof("Job watch handler received not a job, but %+v", obj)
	return nil, false
}
