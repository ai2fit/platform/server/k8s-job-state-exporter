/*
Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cronjobs

import (
	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/klog/v2"
)

// CronjobHandler interface provides a way to act upon signals
// from watcher that only watches the events resource.
type CronjobHandler interface {
	OnAdd(*batchv1.CronJob)
	OnUpdate(*batchv1.CronJob, *batchv1.CronJob)
	OnDelete(*batchv1.CronJob)
}

// cronjobHandlerWrapper is a wrapper of JobHandler for testing proposes.
type cronjobHandlerWrapper struct {
	handler CronjobHandler
}

func newCronjobHandlerWrapper(handler CronjobHandler) *cronjobHandlerWrapper {
	return &cronjobHandlerWrapper{
		handler: handler,
	}
}

func (c *cronjobHandlerWrapper) OnAdd(obj interface{}) {
	if event, ok := c.convert(obj); ok {
		c.handler.OnAdd(event)
	}
}

func (c *cronjobHandlerWrapper) OnUpdate(oldObj interface{}, newObj interface{}) {
	oldJob, oldOk := c.convert(oldObj)
	newJob, newOk := c.convert(newObj)
	if newOk && (oldObj == nil || oldOk) {
		c.handler.OnUpdate(oldJob, newJob)
	}
}

func (c *cronjobHandlerWrapper) OnDelete(obj interface{}) {

	event, ok := obj.(*batchv1.CronJob)

	// When a delete is dropped, the relist will notice a pod in the store not
	// in the list, leading to the insertion of a tombstone object which contains
	// the deleted key/value. Note that this value might be stale.
	if !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			klog.V(2).Infof("Object is neither event nor tombstone: %+v", obj)
			return
		}
		event, ok = tombstone.Obj.(*batchv1.CronJob)
		if !ok {
			klog.V(2).Infof("Tombstone contains object that is not a pod: %+v", obj)
			return
		}
	}

	c.handler.OnDelete(event)
}

// Coverts an object of any type to a batchv1.Cronjob if applicable.
func (c *cronjobHandlerWrapper) convert(obj interface{}) (*batchv1.CronJob, bool) {
	if job, ok := obj.(*batchv1.CronJob); ok {
		return job, true
	}
	klog.V(2).Infof("Job watch handler received not a job, but %+v", obj)
	return nil, false
}
