/*
Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/sinks/mongodb"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/klog/v2"

	"github.com/peterbourgon/ff/v3"
)

func newSystemStopChannel() chan struct{} {
	ch := make(chan struct{})
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		sig := <-c
		klog.Infof("Received signal %s, terminating", sig.String())

		// Close stop channel to make sure every goroutine will receive stop signal.
		close(ch)
	}()

	return ch
}

func newKubernetesClient() (kubernetes.Interface, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("failed to create in-cluster config: %v", err)
	}
	// Use protobufs for communication with apiserver.
	config.ContentType = "application/vnd.kubernetes.protobuf"

	return kubernetes.NewForConfig(config)
}

func main() {
	fs := flag.NewFlagSet("state-exporter", flag.ContinueOnError)
	var (
		namespace          = fs.String("namespace", "default", "Namespace to watch for jobs")
		label              = fs.String("label", "type=job", "Label to filter jobs for")
		jobType            = fs.String("job-type", "jobs", "Job-type to monitor. One of: jobs, cronjobs")
		resyncPeriod       = fs.Duration("resync-period", 1*time.Minute, "Reflector resync period")
		prometheusEndpoint = fs.String("prometheus-endpoint", ":80", "Endpoint on which to expose Prometheus http handler")
		mongodbEndpoint    = fs.String("mongodb-server", "mongodb://localhost:27017/", "Mongodb endpoint")
		mongodbDb          = fs.String("mongodb-db", "execution-controller", "Database for writes")
		mongodbCollection  = fs.String("mongodb-collection", "jobs", "Collection for writes")
	)

	klog.InitFlags(fs)
	defer klog.Flush()

	if err := ff.Parse(fs, os.Args[1:], ff.WithEnvVars()); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	client, err := newKubernetesClient()
	if err != nil {
		klog.Fatalf("Failed to initialize kubernetes client: %v", err)
	}

	config := mongodb.DefaultMongoDbConf()
	config.Endpoint = *mongodbEndpoint
	config.Db = *mongodbDb
	config.Collection = *mongodbCollection

	stopCh := newSystemStopChannel()

	// Expose the Prometheus http endpoint
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		klog.Fatalf("Prometheus monitoring failed: %v", http.ListenAndServe(*prometheusEndpoint, nil))
	}()

	switch *jobType {
	case "jobs":
		sink, err := mongodb.NewJobSink(client, config)
		if err != nil {
			klog.Fatalf("Failed to initialize mongodb output: %v", err)
		}

		jobExporter := newJobExporter(client, sink, *resyncPeriod, *namespace, *label)
		jobExporter.Run(stopCh)
	case "cronjobs":
		sink, err := mongodb.NewCronjobSink(client, config)
		if err != nil {
			klog.Fatalf("Failed to initialize mongodb output: %v", err)
		}

		jobExporter := newCronjobExporter(client, sink, *resyncPeriod, *namespace, *label)
		jobExporter.Run(stopCh)
	default:
		klog.Fatalf("Job-type %s not supported, must be one of: jobs, cronjobs", jobType)
	}
}
