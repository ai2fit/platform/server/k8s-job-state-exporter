/*
Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"time"

	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/sinks"
	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/utils"
	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/watchers"
	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/watchers/cronjobs"

	"k8s.io/client-go/kubernetes"
)

type cronjobExporter struct {
	sink    sinks.CronjobSink
	watcher watchers.Watcher
}

func (e *cronjobExporter) Run(stopCh <-chan struct{}) {
	utils.RunConcurrentlyUntil(stopCh, e.sink.Run, e.watcher.Run)
}

func newCronjobExporter(client kubernetes.Interface, sink sinks.CronjobSink, resyncPeriod time.Duration, namespace string, label string) *cronjobExporter {
	return &cronjobExporter{
		sink:    sink,
		watcher: createCronjobWatcher(client, sink, resyncPeriod, namespace, label),
	}
}

func createCronjobWatcher(client kubernetes.Interface, sink sinks.CronjobSink, resyncPeriod time.Duration, namespace string, label string) watchers.Watcher {
	return cronjobs.NewCronjobWatcher(client, &cronjobs.CronjobWatcherConfig{
		OnList:       sink.OnList,
		ResyncPeriod: resyncPeriod,
		Handler:      sink,
		Namespace:    namespace,
		Label:        label,
	})
}
