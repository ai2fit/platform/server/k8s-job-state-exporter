package sinks

import (
	"time"

	batchv1 "k8s.io/api/batch/v1"
)

type EventType string

const (
	OnAdd    EventType = "OnAdd"
	OnUpdate EventType = "OnUpdate"
	OnDelete EventType = "OnDelete"
	OnList   EventType = "OnList"
)

type JobEvent struct {
	Job       batchv1.Job
	Timestamp time.Time
	EventType EventType
}

type CronjobEvent struct {
	Cronjob   batchv1.CronJob
	Timestamp time.Time
	EventType EventType
}
