/*
Copyright 2018 Alauda Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mongodb

import (
	"context"
	"sort"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog/v2"

	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/sinks"
)

type MongoDbCronjobSink struct {
	config          *MongoDbConf
	mongoClient     *mongo.Client
	k8sClient       kubernetes.Interface
	beforeFirstList bool
	currentBuffer   []*sinks.CronjobEvent
	logEntryChannel chan *sinks.CronjobEvent
	// Channel for controlling how many requests are being sent at the same
	// time. It's empty initially, each request adds an object at the start
	// and takes it out upon completion. Channel's capacity is set to the
	// maximum level of parallelism, so any extra request will lock on addition.
	concurrencyChannel chan struct{}
	timer              *time.Timer
	fakeTimeChannel    chan time.Time
}

func NewCronjobSink(k8sClient kubernetes.Interface, config *MongoDbConf) (*MongoDbCronjobSink, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(config.Endpoint))
	if err != nil {
		klog.Errorf("Error create mongodb(%s) output %v", config.Endpoint, err)
		return nil, err
	}

	klog.Infof("New cronjobSink connection inited.")

	return &MongoDbCronjobSink{
		mongoClient:        mongoClient,
		k8sClient:          k8sClient,
		beforeFirstList:    true,
		logEntryChannel:    make(chan *sinks.CronjobEvent, config.MaxBufferSize),
		config:             config,
		currentBuffer:      []*sinks.CronjobEvent{},
		timer:              nil,
		fakeTimeChannel:    make(chan time.Time),
		concurrencyChannel: make(chan struct{}, config.MaxConcurrency),
	}, nil
}

func (ms *MongoDbCronjobSink) OnAdd(cronjob *batchv1.CronJob) {
	ReceivedEntryCount.Inc()
	klog.Infof("OnAdd %v", cronjob)
	ms.logEntryChannel <- &sinks.CronjobEvent{Cronjob: *cronjob, Timestamp: time.Now(), EventType: sinks.OnAdd}
}

func (ms *MongoDbCronjobSink) OnUpdate(oldCronjob *batchv1.CronJob, newCronjob *batchv1.CronJob) {
	klog.Infof("OnUpdate %v", newCronjob)
	ReceivedEntryCount.Inc()
	ms.logEntryChannel <- &sinks.CronjobEvent{Cronjob: *newCronjob, Timestamp: time.Now(), EventType: sinks.OnUpdate}
}

func (ms *MongoDbCronjobSink) OnDelete(cronjob *batchv1.CronJob) {
	klog.Infof("OnUpdate %v", cronjob)
	ReceivedEntryCount.Inc()
	ms.logEntryChannel <- &sinks.CronjobEvent{Cronjob: *cronjob, Timestamp: time.Now(), EventType: sinks.OnDelete}
}

func (ms *MongoDbCronjobSink) OnList(list *batchv1.CronJobList) {
	klog.Infof("OnList %v", list)
	if ms.beforeFirstList {
		ms.beforeFirstList = false
	}

	time := time.Now()
	for _, v := range list.Items {
		ms.logEntryChannel <- &sinks.CronjobEvent{Cronjob: v, Timestamp: time, EventType: sinks.OnList}
	}
}

func (ms *MongoDbCronjobSink) Run(stopCh <-chan struct{}) {
	klog.Info("Starting MongoDb sink")
	for {
		select {
		case entry := <-ms.logEntryChannel:
			ms.currentBuffer = append(ms.currentBuffer, entry)
			if len(ms.currentBuffer) >= ms.config.MaxBufferSize {
				ms.flushBuffer()
			} else if len(ms.currentBuffer) == 1 {
				ms.setTimer()
			}
		case <-ms.getTimerChannel():
			ms.flushBuffer()
		case <-stopCh:
			klog.Info("Mongodb cronjobSink received stop signal, waiting for all requests to finish")
			klog.Info("All requests to Mongodb finished, exiting Mongodb sink")
			return
		}
	}
}

func (ms *MongoDbCronjobSink) flushBuffer() {
	entries := ms.currentBuffer
	ms.currentBuffer = nil
	ms.concurrencyChannel <- struct{}{}
	go ms.sendEntries(entries)
}
func (ms *MongoDbCronjobSink) sendEntries(entries []*sinks.CronjobEvent) {
	klog.V(4).Infof("Sending %d entries to Mongodb", len(entries))

	// bulkRequest := es.mongoClient.Bulk()
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Timestamp.Before(entries[j].Timestamp)
	})

	jobs := make(map[string]*Job)
	for _, jobEvent := range entries {
		k8sCronjob := jobEvent.Cronjob
		klog.Infof("Orig obj: %v", k8sCronjob)
		newJobs := K8sCronJobToJobs(ms.k8sClient, &k8sCronjob)
		klog.Infof("Jobs: %v", newJobs)
		for _, v := range newJobs {
			jobs[v.Id] = v
		}
	}

	models := make([]mongo.WriteModel, 0)
	for _, job := range jobs {
		models = append(models, mongo.NewUpdateOneModel().SetUpsert(true).SetFilter(bson.D{{Key: "id", Value: job.Id}}).SetUpdate(bson.D{{Key: "$set", Value: bson.D{{Key: "cronjobId", Value: job.cronjobId}, {Key: "executions", Value: job.Executions}}}}))
		klog.Infof("Executions: %v", job.Executions)
	}

	collection := ms.mongoClient.Database(ms.config.Db).Collection(ms.config.Collection)

	_, err := collection.BulkWrite(context.TODO(), models)
	if err != nil {
		klog.Errorf("save events error: %v", err)
	}

	// measureLatencyOnSuccess(entries)
	SuccessfullySentEntryCount.Add(float64(len(entries)))

	<-ms.concurrencyChannel

	klog.V(4).Infof("Successfully sent %d entries to Mongodb", len(entries))
}

func (es *MongoDbCronjobSink) setTimer() {
	if es.timer == nil {
		es.timer = time.NewTimer(es.config.FlushDelay)
	} else {
		es.timer.Reset(es.config.FlushDelay)
	}
}

func (es *MongoDbCronjobSink) getTimerChannel() <-chan time.Time {
	if es.timer == nil {
		return es.fakeTimeChannel
	}
	return es.timer.C
}
