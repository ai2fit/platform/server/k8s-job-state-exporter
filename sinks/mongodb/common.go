/*
Copyright 2018 Alauda Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mongodb

import (
	"time"

	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/sinks"
)

type MongoDbConf struct {
	sinks.SinkCommonConf
	Endpoint   string
	User       string
	Password   string
	Db         string
	Collection string
}

func DefaultMongoDbConf() *MongoDbConf {
	return &MongoDbConf{
		SinkCommonConf: sinks.SinkCommonConf{
			FlushDelay:     5 * time.Second,
			MaxBufferSize:  100,
			MaxConcurrency: 1,
		},
	}
}
