/*
Copyright 2018 Alauda Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package mongodb

import (
	"context"
	"fmt"
	"sort"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog/v2"

	"gitlab.oldworldcomputing.com/singularity/server/k8s-state-exporter/sinks"
)

type MongoDbJobSink struct {
	config          *MongoDbConf
	mongoClient     *mongo.Client
	k8sClient       kubernetes.Interface
	beforeFirstList bool
	currentBuffer   []*sinks.JobEvent
	jobEventChannel chan *sinks.JobEvent
	// Channel for controlling how many requests are being sent at the same
	// time. It's empty initially, each request adds an object at the start
	// and takes it out upon completion. Channel's capacity is set to the
	// maximum level of parallelism, so any extra request will lock on addition.
	concurrencyChannel chan struct{}
	timer              *time.Timer
	fakeTimeChannel    chan time.Time
}

func NewJobSink(k8sClient kubernetes.Interface, config *MongoDbConf) (*MongoDbJobSink, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(config.Endpoint))
	if err != nil {
		klog.Errorf("Error create mongodb(%s) output %v", config.Endpoint, err)
		return nil, err
	}

	klog.Infof("NewMongoDB sink connection inited.")

	return &MongoDbJobSink{
		mongoClient:        mongoClient,
		k8sClient:          k8sClient,
		beforeFirstList:    true,
		jobEventChannel:    make(chan *sinks.JobEvent, config.MaxBufferSize),
		config:             config,
		currentBuffer:      []*sinks.JobEvent{},
		timer:              nil,
		fakeTimeChannel:    make(chan time.Time),
		concurrencyChannel: make(chan struct{}, config.MaxConcurrency),
	}, nil
}

func (ms *MongoDbJobSink) OnAdd(job *batchv1.Job) {
	ReceivedEntryCount.Inc()
	klog.V(3).Infof("OnAdd %v", job)
	ms.jobEventChannel <- &sinks.JobEvent{Job: *job, Timestamp: time.Now(), EventType: sinks.OnAdd}
}

func (ms *MongoDbJobSink) OnUpdate(oldJob *batchv1.Job, newJob *batchv1.Job) {
	ReceivedEntryCount.Inc()
	klog.V(3).Infof("OnUpdate %v", newJob)
	ms.jobEventChannel <- &sinks.JobEvent{Job: *newJob, Timestamp: time.Now(), EventType: sinks.OnUpdate}
}

func (ms *MongoDbJobSink) OnDelete(job *batchv1.Job) {
	ReceivedEntryCount.Inc()
	klog.V(3).Infof("OnDelete %v", job)
	ms.jobEventChannel <- &sinks.JobEvent{Job: *job, Timestamp: time.Now(), EventType: sinks.OnDelete}
}

func (ms *MongoDbJobSink) OnList(list *batchv1.JobList) {
	klog.V(3).Infof("OnList %v", list)
	if ms.beforeFirstList {
		ms.beforeFirstList = false
	}

	time := time.Now()
	for _, v := range list.Items {
		ms.jobEventChannel <- &sinks.JobEvent{Job: v, Timestamp: time, EventType: sinks.OnList}
	}
}

func (ms *MongoDbJobSink) Run(stopCh <-chan struct{}) {
	klog.V(3).Info("Starting MongoDb sink")
	for {
		select {
		case jobEvent := <-ms.jobEventChannel:
			ms.currentBuffer = append(ms.currentBuffer, jobEvent)
			if len(ms.currentBuffer) >= ms.config.MaxBufferSize {
				ms.flushBuffer()
			} else if len(ms.currentBuffer) == 1 {
				ms.setTimer()
			}
		case <-ms.getTimerChannel():
			ms.flushBuffer()
		case <-stopCh:
			klog.Info("MongoDb sink received stop signal, waiting for all requests to finish")
			klog.Info("All requests to MongoDb finished, exiting MongoDb sink")
			return
		}
	}
}

func (ms *MongoDbJobSink) flushBuffer() {
	entries := ms.currentBuffer
	ms.currentBuffer = nil
	ms.concurrencyChannel <- struct{}{}
	go ms.sendEntries(entries)
}

func (ms *MongoDbJobSink) sendEntries(events []*sinks.JobEvent) {
	klog.V(4).Infof("Sending %d entries to Mongodb", len(events))

	sort.Slice(events, func(i, j int) bool {
		return events[i].Timestamp.Before(events[j].Timestamp)
	})

	var deleteEvents, otherEvents []*sinks.JobEvent
	for _, event := range events {
		if event.EventType == sinks.OnDelete {
			deleteEvents = append(deleteEvents, event)
		} else {
			otherEvents = append(otherEvents, event)
		}
	}

	collection := ms.mongoClient.Database(ms.config.Db).Collection(ms.config.Collection)

	writeModels := getWriteModelsForOtherJobs(parseJobEvents(ms.k8sClient, otherEvents), collection)
	writeModels = append(writeModels, getWriteModelsForDeletedJobs(deleteEvents, collection)...)

	_, err := collection.BulkWrite(context.TODO(), writeModels)
	if err != nil {
		panic(fmt.Sprintf("Save events error: %v", err))
	}

	measureLatencyOnSuccess(events)
	SuccessfullySentEntryCount.Add(float64(len(events)))

	<-ms.concurrencyChannel

	klog.V(4).Infof("Successfully sent %d entries to Mongodb", len(writeModels))
}

func getWriteModelsForDeletedJobs(deleteEvents []*sinks.JobEvent, collection *mongo.Collection) []mongo.WriteModel {
	writeModels := make([]mongo.WriteModel, 0)
	for _, event := range deleteEvents {

		var doc Job
		err := collection.FindOne(context.TODO(), bson.D{{Key: "id", Value: event.Job.Name}}, options.FindOne().SetProjection(bson.D{{Key: "id", Value: 1}, {Key: "executions", Value: 1}})).Decode(&doc)

		if err != nil {
			panic(err)
		}

		executionsInDb := doc.Executions
		for i, e := range executionsInDb {

			if e.Status != Fail && e.Status != Success {
				klog.V(5).Infof("Setting status of job:execution (%v:%v) to stopped", doc.Id, e.Id)
				executionsInDb[i].StoppedAt = time.Now().Format(time.RFC3339)
				executionsInDb[i].Status = Stopped
			}
		}

		writeModels = append(writeModels, mongo.NewUpdateOneModel().SetUpsert(true).SetFilter(bson.D{{Key: "id", Value: event.Job.Name}}).SetUpdate(bson.D{{Key: "$set", Value: bson.D{{Key: "executions", Value: executionsInDb}}}}))

		klog.V(5).Infof("Job: %v, Executions: %v", doc.Id, executionsInDb)
	}
	return writeModels
}

func getWriteModelsForOtherJobs(jobs map[string]*Job, collection *mongo.Collection) []mongo.WriteModel {
	writeModels := make([]mongo.WriteModel, 0)
	for _, job := range jobs {

		var doc Job
		err := collection.FindOne(context.TODO(), bson.D{{Key: "id", Value: job.Id}}, options.FindOne().SetProjection(bson.D{{Key: "id", Value: 1}, {Key: "executions", Value: 1}})).Decode(&doc)

		var executionsInDb []Execution
		if err != nil {
			if err == mongo.ErrNoDocuments {
				executionsInDb = make([]Execution, 0)
			} else {
				panic(err)
			}
		} else {
			executionsInDb = doc.Executions
		}

		executions := job.Executions
		for _, e := range executionsInDb {
			if !contains(executions, e) {
				executions = append(executions, e)
			}
		}

		sort.Slice(executions, func(i, j int) bool {
			time1, _ := time.Parse(time.RFC3339, executions[i].SubmittedAt)
			time2, _ := time.Parse(time.RFC3339, executions[j].SubmittedAt)
			return time1.Before(time2)
		})

		writeModels = append(writeModels, mongo.NewUpdateOneModel().SetUpsert(true).SetFilter(bson.D{{Key: "id", Value: job.Id}}).SetUpdate(bson.D{{Key: "$set", Value: bson.D{{Key: "executions", Value: executions}}}}))

		klog.V(5).Infof("Job: %v, Executions: %v", job.Id, executions)
	}
	return writeModels
}

func contains(s []Execution, e Execution) bool {
	for _, a := range s {
		if a.Id == e.Id {
			return true
		}
	}
	return false
}

func parseJobEvents(k8sClient kubernetes.Interface, events []*sinks.JobEvent) map[string]*Job {
	jobs := make(map[string]*Job)
	for _, jobEvent := range events {
		k8sJob := jobEvent.Job
		klog.V(5).Infof("Orig obj: %v", k8sJob)

		job := K8sJobToJob(k8sClient, &k8sJob)
		klog.V(5).Infof("Converted Job: %v", job)

		jobs[job.Id] = job
	}

	return jobs
}

func (ms *MongoDbJobSink) setTimer() {
	if ms.timer == nil {
		ms.timer = time.NewTimer(ms.config.FlushDelay)
	} else {
		ms.timer.Reset(ms.config.FlushDelay)
	}
}

func (ms *MongoDbJobSink) getTimerChannel() <-chan time.Time {
	if ms.timer == nil {
		return ms.fakeTimeChannel
	}
	return ms.timer.C
}
