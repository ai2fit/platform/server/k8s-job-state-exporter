package mongodb

import (
	"context"
	"fmt"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog/v2"
)

type Job struct {
	Id         string      `bson:"id,omitempty"`
	cronjobId  string      `bson:"cronjobId,omitempty"`
	Executions []Execution `bson:"executions,omitempty"`
	Canceled   bool        `bson:"canceled"`
}

func (j Job) String() string {
	return fmt.Sprintf("{id: %s, cronjobId: %s, executions: %v}", j.Id, j.cronjobId, j.Executions)
}

type Execution struct {
	Id          string `bson:"id,omitempty"`
	SubmittedAt string `bson:"submittedAt,omitempty"`
	StartedAt   string `bson:"startedAt,omitempty"`
	StoppedAt   string `bson:"stoppedAt,omitempty"`
	Status      Status `bson:"status,omitempty"`
}

func (e Execution) String() string {
	return fmt.Sprintf("{id: %s, submitted: %s, started: %s, stopped: %s, status: %s}", e.Id, e.SubmittedAt, e.StartedAt, e.StoppedAt, e.Status)
}

type Status string

const (
	Pending Status = "pending"
	Running Status = "running"
	Success Status = "success"
	Fail    Status = "fail"
	Stopped Status = "stopped"
	Unknown Status = "unknown"
)

func K8sJobToJob(client kubernetes.Interface, job *batchv1.Job) *Job {
	list, err := client.CoreV1().Pods(job.Namespace).List(context.TODO(), metav1.ListOptions{LabelSelector: "job-name=" + job.Name})
	if err != nil {
		klog.Errorf("Error searching for pod for job(%s): %v", job.Name, err)
		return nil
	}

	executions := make([]Execution, 0)
	for _, v := range list.Items {
		execution := Execution{}
		execution.Id = string(v.UID)
		if !v.CreationTimestamp.IsZero() {
			execution.SubmittedAt = v.GetCreationTimestamp().Format(time.RFC3339)
		}

		switch v.Status.Phase {
		case corev1.PodPending:
			execution.Status = Pending
		case corev1.PodRunning:
			execution.Status = Running
		case corev1.PodSucceeded:
			execution.Status = Success
		case corev1.PodFailed:
			execution.Status = Fail
		default:
			execution.Status = Unknown
		}

		if len(v.Status.ContainerStatuses) == 1 {
			state := v.Status.ContainerStatuses[0].State

			if state.Running != nil {
				execution.StartedAt = state.Running.StartedAt.Format(time.RFC3339)
			} else if state.Terminated != nil {
				execution.StartedAt = state.Terminated.StartedAt.Format(time.RFC3339)
				execution.StoppedAt = state.Terminated.FinishedAt.Format(time.RFC3339)
			}
		}

		executions = append(executions, execution)
	}

	return &Job{Id: job.Name, Executions: executions}
}

func K8sCronJobToJobs(client kubernetes.Interface, cronjob *batchv1.CronJob) []*Job {
	list, err := client.BatchV1().Jobs(cronjob.Namespace).List(context.TODO(), metav1.ListOptions{LabelSelector: "cronjob-name=" + cronjob.Name})
	if err != nil {
		klog.Errorf("Error searching for jobs for cronjob(%s): %v", cronjob.Name, err)
		return nil
	}

	jobs := make([]*Job, 0)
	for _, v := range list.Items {
		job := K8sJobToJob(client, &v)
		job.cronjobId = cronjob.Name
		jobs = append(jobs, K8sJobToJob(client, &v))
	}

	return jobs
}
